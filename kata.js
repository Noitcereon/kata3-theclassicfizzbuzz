"use strict"

function fizzBuzzConsole(iterationTimes, fizzNumber = 3, buzzNumber = 5){
    if(typeof(iterationTimes) !== "number" || isNaN(iterationTimes)){
        console.warn("Invalid input");
    }
    iterationTimes++; // To make it more intuitive to use. (write 10, iterate up to and including 10)
    for (let i = 1; i < iterationTimes; i++) {

        if(i % fizzNumber === 0 && i % buzzNumber === 0){
            console.log("FizzBuzz");
            continue;
        }
        if(i % buzzNumber === 0){
            console.log("Buzz");
            continue;
        }35
        if(i % fizzNumber === 0){
            console.log("Fizz");
            continue;
        }
        console.log(i);
    }
}

fizzBuzzConsole(30);



// 1. Check if its dividable by both 3 and 5
    // Print FizzBuzz
// 2. Check if its dividable by 5
    // Print Buzz
// 3. Check if its dividable by 3
    // Print Fizz

// 4 Print number if none of the above.


 // print all numbers
// if it is dividable by 3 return Fizz
// if it is dividable by 5 return Buzz
// if it is dividable by both 3 and 5 return FizzBuzz

